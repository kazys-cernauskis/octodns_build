FROM alpine:3.11
LABEL maintainer=kazys@outlook.com

RUN apk add python3
RUN pip3 install octodns

CMD ["bin/ash"]